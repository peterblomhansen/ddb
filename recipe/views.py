# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic import DetailView
from .models import Section, Broed, KornType, Trin, Korn, Kornsort, OtherIngredient
from django.views.generic.detail import SingleObjectMixin
from django.template.defaultfilters import slugify
from django.http import HttpResponseRedirect, HttpResponse, HttpRequest

# Create your views here.


request = HttpRequest()



class SectionView(DetailView):

    model = Section
    template_name = 'ddb/section.html'
    slug_field = "slug_field"
    
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(SectionView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['menu'] = Section.objects.all()
        context['section'] = self.get_object()
        
        if context['section'].overskrift == "Opskrifter":
            context['recipes'] = Broed.objects.all()

        if context['section'].overskrift == "Melskema":        
            context['mel'] = KornType.objects.all()
            context['kornsorter'] = Kornsort.objects.all()            
        
        return context



class RecipeView(DetailView):

    model = Broed
    template_name = 'ddb/recipe.html'
    slug_field = slugify("navn")
    
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(RecipeView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['menu'] = Section.objects.all()
        context['recipe'] = self.get_object()
        context['trin'] = Trin.objects.filter(broed=self.get_object().id)
        context['korn'] = Korn.objects.filter(trin__broed=self.get_object().id)
        context['other'] = OtherIngredient.objects.filter(trin__broed=self.get_object().id).order_by('sortering') 
       
        return context



class BroedView(DetailView):

    model = Broed
    context_object_name = "recipe"
    template_name = 'ddb/recipe2.html'
    slug_field = "navn_url"

    def get_context_data(self, **kwargs):

        context = super(BroedView, self).get_context_data(**kwargs)
        context['host'] = request
        context['test'] = "request"        
        context['menu'] = Section.objects.all()        
        return context
    