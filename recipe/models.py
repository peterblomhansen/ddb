# -*- coding: utf-8 -*

from django.db import models
from django.contrib import admin
from tinymce.models import HTMLField
from image_cropping import ImageRatioField
from django.utils.text import slugify

# Create your models here.

class GrundDej(models.Model):
    navn = models.CharField(max_length=255)
    beskrivelse = models.TextField()

    class Meta:
	    verbose_name = "Grunddej"
	    verbose_name_plural = "Grunddeje"

    def __str__(self):
         return self.navn


class Broed(models.Model):
    navn = models.CharField(max_length=255)
    manchet = models.CharField(max_length=255,blank=True,null=True)
    beskrivelse = HTMLField(blank=True,null=True)
    variation = HTMLField(blank=True,null=True)    
    grund_dej = models.ForeignKey(GrundDej)
    billede = models.ImageField(blank=True,null=True)
    cropping = ImageRatioField('billede', '1000x1000', allow_fullsize=True, free_crop=True)
    navn_url = models.SlugField(blank=True, null=True)
    
    class Meta:
        verbose_name = "Brød"
        verbose_name_plural = "Brød"
	
    def __str__(self):
         return self.navn

    def save(self):
        #import markdown
       # self.content = markdown.markdown(self.content_md)
        self.navn_url = slugify(self.navn,allow_unicode=True)
        super(Broed, self).save() # Call the "real" save() method.         
    
    
class KornType(models.Model):
    navn = models.CharField(max_length=255)
    beskrivelse = models.TextField()
    sort = models.ForeignKey("Kornsort")
    finhed = models.ForeignKey("Finhed")
    haeve_evne = models.ForeignKey("Haeveevne")
    
    class Meta:
        verbose_name = "Meltype"
        verbose_name_plural = "Meltyper"

    def __str__(self):
         return self.navn


class Korn(models.Model):
    korn_type = models.ForeignKey("KornType")
    maengde = models.IntegerField()
    trin = models.ForeignKey("Trin",related_name="korn") 
    
    class Meta:
        verbose_name = "Korn"
        verbose_name_plural = "Korn"
	
    def __str__(self):
         return str(self.maengde) + "g " + self.korn_type.navn

	
class Trin(models.Model):
    nummer = models.IntegerField()
    overskrift = models.CharField(max_length=255)
    beskrivelse = HTMLField(blank=True,null=True)
    broed = models.ForeignKey("Broed",related_name="trin")
    bage_temperatur = models.IntegerField(blank=True,null=True)
    bage_tid_minutter = models.DecimalField(decimal_places=2,max_digits=4,blank=True,null=True)

    class Meta:
        verbose_name = "Trin"
        verbose_name_plural = "Trin"
        ordering = ["nummer"]        
        
    def __str__(self):
         return self.overskrift
         

class Section(models.Model):
    overskrift = models.CharField(max_length=255)
    indhold = HTMLField(blank=True,null=True)
    billede = models.ImageField(blank=True,null=True)
    cropping = ImageRatioField('billede', '1000x1000', allow_fullsize=True, free_crop=True)
    sortering = models.IntegerField()
    slug_field = models.CharField(max_length=255,blank=True,null=True)
    
    class Meta:
        verbose_name = "Sektion"
        verbose_name_plural = "Sektioner"   
        ordering = ["sortering"]
    
    def __str__(self):
         return self.overskrift


class Kornsort(models.Model):
    navn = models.TextField(default="kornsort-navn")
    beskrivelse = HTMLField(blank=True,null=True)
    
    class Meta:
        verbose_name = "Kornsort"
        verbose_name_plural = "Kornsorter"    

    def __str__(self):
         return self.navn

class Finhed(models.Model):
    beskrivelse = models.TextField()
    
    class Meta:
        verbose_name = "Finhed"
        verbose_name_plural = "Finheder"   

    def __str__(self):
         return self.beskrivelse
	
		
class Haeveevne(models.Model):
    beskrivelse = models.TextField()
    
    class Meta:
        verbose_name = "Hæveevne"
        verbose_name_plural = "Hæveevner"   

    def __str__(self):
         return self.beskrivelse



class Ingredient(models.Model):
    navn = models.CharField(max_length=50)
    beskrivelse = models.TextField(blank=True,null=True)
    maengde_type = models.CharField(max_length=50)
    ingredient_type = models.CharField(max_length=50)


    class Meta:
        verbose_name = "Ingrediens"
        verbose_name_plural = "Ingredienser"

    def __str__(self):
         return self.navn


class OtherIngredient(models.Model):
    navn = models.ForeignKey("Ingredient")
    sortering = models.IntegerField(blank=True,null=True)
#    navn = models.CharField(max_length=255)
    maengde = models.IntegerField()
#    maengde_type = models.CharField(max_length=255)
    trin = models.ForeignKey("Trin",related_name="other_ingredient") 
    
    
    class Meta:
        verbose_name = "Anden ingrediens"
        verbose_name_plural = "Andre ingredienser"
	
    def __str__(self):
         return str(self.maengde) + " " + str(self.navn.maengde_type) + " " + str(self.navn)


