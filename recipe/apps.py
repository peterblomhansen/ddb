from django.apps import AppConfig

class RecipeConfig(AppConfig):
    name = 'recipe'
    verbose_name = "Opskrift"
    verbose_name_plural = "Opskrifter"
