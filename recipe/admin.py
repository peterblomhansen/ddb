from django.contrib import admin
from image_cropping import ImageCroppingMixin
from nested_inline.admin import NestedStackedInline, NestedTabularInline, NestedModelAdmin

# from django.forms import *
# from django.db.models import *
# from tinymce.widgets import TinyMCE

# Register your models here.
from .models import GrundDej, Broed, Korn, Trin, KornType, Section, Kornsort, Haeveevne, Finhed, OtherIngredient, Ingredient


class OtherAdmin(NestedTabularInline):
    model = OtherIngredient
    extra = 0
    fk_name = 'trin'

class KornAdmin(NestedTabularInline):
    model = Korn
    extra = 0
    fk_name = 'trin'

class TrinInline(NestedStackedInline):
    model = Trin
    extra = 0
    fk_name = 'broed'
    inlines = [KornAdmin,OtherAdmin]

class BroedAdmin(ImageCroppingMixin,NestedModelAdmin):
    exclude = ('navn_url',)
    inlines = [TrinInline]
'''

class OtherAdmin(NestedTabularInline):
    model = OtherIngredient
    extra = 0
    fk_name = 'trin'

class KornAdmin(NestedTabularInline):
    model = Korn
    extra = 0
    fk_name = 'trin'

class TrinInline(NestedStackedInline):
    model = Trin
    extra = 0
    fk_name = 'broed'
    inlines = [KornAdmin,OtherAdmin]

class BroedAdmin(ImageCroppingMixin,admin.ModelAdmin):
    exclude = ["navn_url"]
    inlines = [TrinInline]
'''

#class BroedAdmin(ImageCroppingMixin, admin.ModelAdmin):
#    pass

class SectionAdmin(ImageCroppingMixin, admin.ModelAdmin):
    pass



#class ProductionForm(forms.ModelForm):
#    some_field = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 10}))
#
#    class Meta:
#        model = Production

 #class ProductionAdmin(admin.

admin.site.register(Section,SectionAdmin)
admin.site.register(Broed, BroedAdmin)
admin.site.register(GrundDej)
#admin.site.register(Broed,BroedAdmin)
admin.site.register(KornType)
admin.site.register(Korn)
admin.site.register(Trin)
#admin.site.register(Section)
admin.site.register(Kornsort)
admin.site.register(Haeveevne)
admin.site.register(Finhed)
admin.site.register(OtherIngredient)
admin.site.register(Ingredient)
