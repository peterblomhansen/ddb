from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

#from blog.views import PostList

from . import views
from .views import SectionView, RecipeView,BroedView

urlpatterns = [

    url(r'^$', SectionView.as_view(),{ 'pk': 1}, name="index"),
    url(r'^(?P<slug>[-\w]+)/$', SectionView.as_view(), name="section"),
  #  url(r'^opskrift/(?P<pk>[0-9]+)/$', RecipeView.as_view(), name="recipe"),
    url(r'^opskrifter/(?P<slug>[-\w]+)/$', BroedView.as_view(), name="recipe2"),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

