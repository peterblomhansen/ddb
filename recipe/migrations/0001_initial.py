# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Broed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('navn', models.CharField(max_length=255)),
                ('manchet', models.CharField(null=True, max_length=255, blank=True)),
                ('beskrivelse', models.TextField(null=True, blank=True)),
                ('billede', models.ImageField(null=True, upload_to='', blank=True)),
            ],
            options={
                'verbose_name': 'Brød',
                'verbose_name_plural': 'Brød',
            },
        ),
        migrations.CreateModel(
            name='Finhed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('beskrivelse', models.TextField()),
            ],
            options={
                'verbose_name': 'Finhed',
                'verbose_name_plural': 'Finheder',
            },
        ),
        migrations.CreateModel(
            name='GrundDej',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('navn', models.CharField(max_length=255)),
                ('beskrivelse', models.TextField()),
            ],
            options={
                'verbose_name': 'Grunddej',
                'verbose_name_plural': 'Grunddeje',
            },
        ),
        migrations.CreateModel(
            name='Haeveevne',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('beskrivelse', models.TextField()),
            ],
            options={
                'verbose_name': 'Hæveevne',
                'verbose_name_plural': 'Hæveevner',
            },
        ),
        migrations.CreateModel(
            name='Korn',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('maengde', models.IntegerField()),
            ],
            options={
                'verbose_name': 'Korn',
                'verbose_name_plural': 'Korn',
            },
        ),
        migrations.CreateModel(
            name='Kornsort',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('beskrivelse', models.TextField()),
            ],
            options={
                'verbose_name': 'Kornsort',
                'verbose_name_plural': 'Kornsorter',
            },
        ),
        migrations.CreateModel(
            name='KornType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('navn', models.CharField(max_length=255)),
                ('beskrivelse', models.TextField()),
                ('finhed', models.ForeignKey(to='recipe.Finhed')),
                ('haeve_evne', models.ForeignKey(to='recipe.Haeveevne')),
                ('sort', models.ForeignKey(to='recipe.Kornsort')),
            ],
            options={
                'verbose_name': 'Korntype',
                'verbose_name_plural': 'Korntyper',
            },
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('overskrift', models.CharField(max_length=255)),
                ('indhold', tinymce.models.HTMLField(null=True, blank=True)),
                ('billede', models.ImageField(null=True, upload_to='', blank=True)),
                ('order', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Trin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('nummer', models.IntegerField()),
                ('overskrift', models.CharField(max_length=255)),
                ('beskrivelse', models.TextField()),
                ('bage_temperatur', models.IntegerField(null=True, blank=True)),
                ('bage_tid_minutter', models.DecimalField(null=True, decimal_places=2, max_digits=4, blank=True)),
                ('broed', models.ForeignKey(to='recipe.Broed')),
            ],
            options={
                'verbose_name': 'Trin',
                'verbose_name_plural': 'Trin',
            },
        ),
        migrations.AddField(
            model_name='korn',
            name='korn_type',
            field=models.ForeignKey(to='recipe.KornType'),
        ),
        migrations.AddField(
            model_name='korn',
            name='trin',
            field=models.ForeignKey(to='recipe.Trin'),
        ),
        migrations.AddField(
            model_name='broed',
            name='grund_dej',
            field=models.ForeignKey(to='recipe.GrundDej'),
        ),
    ]
