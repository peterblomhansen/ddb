# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0002_auto_20151125_1651'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='slug_field',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='section',
            name='cropping',
            field=image_cropping.fields.ImageRatioField('billede', '1000x1000', adapt_rotation=False, size_warning=False, hide_image_field=False, help_text=None, free_crop=True, allow_fullsize=True, verbose_name='cropping'),
        ),
    ]
