# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='section',
            options={'verbose_name_plural': 'Sektioner', 'ordering': ['sortering'], 'verbose_name': 'Sektion'},
        ),
        migrations.RenameField(
            model_name='section',
            old_name='order',
            new_name='sortering',
        ),
        migrations.AddField(
            model_name='broed',
            name='cropping',
            field=image_cropping.fields.ImageRatioField('billede', '430x360', size_warning=False, free_crop=False, allow_fullsize=False, help_text=None, adapt_rotation=False, verbose_name='cropping', hide_image_field=False),
        ),
        migrations.AddField(
            model_name='section',
            name='cropping',
            field=image_cropping.fields.ImageRatioField('billede', '430x360', size_warning=False, free_crop=False, allow_fullsize=False, help_text=None, adapt_rotation=False, verbose_name='cropping', hide_image_field=False),
        ),
    ]
